import json

from time import strftime, sleep

import  requests

def request_url():
    url = 'https://j1.pupuapi.com/client/product/storeproduct/detail/4dcdeca2-f5a3-4be8-9e2f-e099889a23a0/cbfa34f9-9f12-4200-a683-54b53695adb2'
    head = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat'
    }
    res = requests.get(url, headers=head)
    ppshop = json.loads(res.text)
    name = ppshop["data"]["name"]  # 商品名字
    spec = ppshop["data"]["spec"]  # 规格
    price = str(int(ppshop["data"]["price"]) / 100)  # 折扣价
    market_price = str(int(ppshop["data"]["market_price"]) / 100)  # 原价
    share_content = ppshop["data"]["share_content"]  # 详细内容
    print("-------------商品：" + name + "-------------")
    print("规格：" + spec)
    print("原价：" + price)
    print("原价/折扣价：" + price + "/" + market_price)
    print("详细内容：" + share_content)

def time():  # 获取时间
    url = 'https://j1.pupuapi.com/client/product/storeproduct/detail/4dcdeca2-f5a3-4be8-9e2f-e099889a23a0/cbfa34f9-9f12-4200-a683-54b53695adb2'
    head = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat'
    }
    res = requests.get(url, headers=head)
    ppshop = json.loads(res.text)
    name = ppshop["data"]["name"]  # 商品名字
    price = str(int(ppshop["data"]["price"]) / 100)  # 折扣价
    print("-------------" + name + "-------------")
    try:  #抛出异常5
        while (True):
            nowTimeAndPrint = strftime('%Y' + '-' + '%m' + '-' + '%d' + ' %H:%M:%S,价格为' + price)
            print(nowTimeAndPrint)
            sleep(5)
    except:
        print("程序结束")

if __name__ == '__main__':
    request_url()
    print("\n")
    time()